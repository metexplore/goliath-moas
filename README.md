# Goliath-MoAs
This git repository contains metabolic subnetworks represented the potentially modulated part of the whole metabolic network under the different exposure conditions tested in the Goliath project.

## Repository summary
This repository contains the following files:

```
DiscrimantMetabolites_hMSC_BPA.txt
DiscrimantMetabolites_hMSC_PFOA.txt
DiscrimantMetabolites_hMSC_TBT.txt
DiscrimantMetabolites_hMSC_ppDDE.txt
DiscrimantMetabolites_HPR116_2d_BPA.txt
DiscrimantMetabolites_HPR116_10d_PFOA.txt

SubNetwork_hMSC_BPA.txt
SubNetwork_hMSC_TBT.txt
SubNetwork_hMSC_TCS.txt
SubNetwork_hMSC_ppDDE.txt
SubNetwork_HPR116_2d_BPA.txt
SubNetwork_HPR116_10d_PFOA.txt
```
The first 8 files are the discriminant metabolites identified in hepatocytes (HPR116 cells) and adypocytes (hMSC cells) under exposure to the differents MDCs.
The last 8 files are the produced subnetworks (each provided as a list of reactions and associated visualisation link in MetExplore) representing the reactions that are potentially modulated in the tested exposure conditions.
The following table summarize condition exposures and statistics for the generated subnetworks.  

| cell type   | MDC   | Exposure duration | Exposure concentration | nb. metabolites mapped | nb. reactions in subnetwork |
|-------------|------:|------------------:|-----------------------:|-----------------------:|----------------------------:|
| HPR116      | BPA   |     2d            |     1 nM               |       6                |       48                    |
| HPR116      | PFOA  |    10d            |   100 µM               |      14                |      222                    |
| hMSC        | BPA   |    14d            |   100 µM               |      14                |      225                    |
| hMSC        | TBT   |    14d            |    50 nM               |      40                |      646                    |
| hMSC        | TCS   |    14d            |    10 µM               |      19                |      277                    |
| hMSC        | ppDDE |    14d            |    30 µM               |      19                |      309                    |

```
HumanGEM14_SideCompounds.tsv

```
The tsv file provides the list of side compounds metabolites, used as input for running the Galaxy workflow presented below.

## Method overview

The subnetworks were generated using the met4j tool (https://github.com/MetExplore/met4j).
The workflow uses as inputs:
- the cell-specific models built from transcriptomics data (provided in repository https://forgemia.inra.fr/metexplore/goliath-models) 
- the network identifiers corresponding to the metabolites identified as modulated in each exposure condition (provided in this repository)
- the lists of metabolites to exclude (also provided in this repository).

The computational method used was previously developed by (Frainay C. & Jourdan F. Brief Bioinform. 2017;18: 43–56) and implemented in the Met4J library, available as a Galaxy tool. 
It consists in automatically extracting a subset of reactions (sub-network), among the thousands of reactions contained in the initial network, which connects the modulated metabolites. 
This algorithm uses metabolic distances (number of reactions) between modulated metabolites in the fingerprint to find out cascades of reactions of interest. 
It results in sub-networks containing tens to hundreds of reactions potentially related to the exposure. 
Of note, to avoid biologically irrelevant connections, the algorithm removes highly connected metabolites (called "side compounds") and artificial pool reactions. 
The extraction is performed on undirected graph, with the k-shortest path algorithm (k=1).

## Visualizations
List of reactions outputted by this workflow can then be visualized as subnetworks using the MetExplore webserver (https://metexplore.toulouse.inrae.fr/metexplore2/). 
Below are the html links to the visualizations of the subnetworks corresponding to the exposure conditions detailed above:
- hMSC cells exposed to BPA (14d - 100µM): https://metexplore.toulouse.inrae.fr/userFiles/metExploreViz/index.html?dir=/23b6baf5854e5ca0929214c1f94600c7/networkSaved_1269493215
- hMSC cells exposed to TBT (14d - 50nM): https://metexplore.toulouse.inrae.fr/userFiles/metExploreViz/index.html?dir=/23b6baf5854e5ca0929214c1f94600c7/networkSaved_1443080939 
- hMSC cells exposed to TCS (14d - 10µM): https://metexplore.toulouse.inrae.fr/userFiles/metExploreViz/index.html?dir=/23b6baf5854e5ca0929214c1f94600c7/networkSaved_1724769562
- hMSC cells exposed to ppDDE (14d - 30µM): https://metexplore.toulouse.inrae.fr/userFiles/metExploreViz/index.html?dir=/23b6baf5854e5ca0929214c1f94600c7/networkSaved_76934949 
- HPR116 cells exposed to BPA (2d - 1nM): https://metexplore.toulouse.inrae.fr/userFiles/metExploreViz/index.html?dir=/23b6baf5854e5ca0929214c1f94600c7/networkSaved_620545765
- HPR116 cells exposed to PFOA (10d - 100µM): https://metexplore.toulouse.inrae.fr/userFiles/metExploreViz/index.html?dir=/23b6baf5854e5ca0929214c1f94600c7/networkSaved_3362282


